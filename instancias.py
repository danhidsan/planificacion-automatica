from problemaAscensoresPOO import ProblemaAscensores

"""Instancia ejemplo del enunciado del promblema con solo dos Pasajeros"""


"""Ascensores lentos de la instancia: Dichos Ascensores tendremos que colocarlos en el orden en el que se encuentra 
en el enunciado del problema """

AscensoresLentosEjemplo = ['a1', 'a2']

"""Ascensores Rápidos de la instancia: Añadiremos los ascensores a la lista en el mismo orden en el que se encuentran 
en la instancia """

AscensoresRapidosEjemplo = ['a3', 'a4']  # Ascensores rápidos de la instancia


"""Capacidades de los ascensores: Cada entero es la capacidad de cada ascensor en orden, sumando las listas de 
ascensores: AscensoresLentos + AscensoresRapidos """

CapacidadesAscensoresEjemplo = [3, 3, 2, 1]


"""Pasajeros de la instancia: Hay que añadir los pasajeros de la instancia en el orden que se encuentran en el 
enunciado """

PasajerosEjemplo = ['cura', 'hombre','policia']

"""Plantas de la instancia: Lista de cadenas con los numeros de las plantas en orden creciente"""

PlantasEjemplo = ['0', '1', '2', '3', '4', '5', '6', '7', '8']

"""Plantas Pasajeros en el estado inicial: Las plantas se añadirán en el mismo orden en el que se encuentra los 
pasajeros en la lista PasajerosEjemplo """

PlantasEstadoInicialEjemplo = ['4', '0', '0']

"""Plantas Pasajeros en el estado final: Las plantas se añadirán en el mismo orden en el que se encuentra los 
pasajeros en la lista PasajerosEjemplo """

PlantasEstadoFinalEjemplo = ['6', '8', '7']

"""Planta Ascensores Estado Inicial:Cada capacidad es la planta de cada ascensor en orden, sumando las listas de 
ascensores: AscensoresLentos + AscensoresRapidos """

PlantasAscensoresEstadoInicialEjemplo = ['3', '6', '0', '4']




"""Creamos el problema Instanciando la Clase ProblemaAscensores y pasadole todos los parámetros que hemos creado 
anteriormente """


instancia_ejemplo = ProblemaAscensores(AscensoresLentosEjemplo, AscensoresRapidosEjemplo,
                                       CapacidadesAscensoresEjemplo, PasajerosEjemplo, PlantasEjemplo,
                                       PlantasEstadoInicialEjemplo, PlantasEstadoFinalEjemplo,
                                       PlantasAscensoresEstadoInicialEjemplo)


"""Realizamos las busquedas pertinentes: En este caso la busqueda por defecto será busqueda en profundidad, para probar 
las demas busquedas, comenta dicho codigo y quita el comentario de la busqueda que desea """


#Busqueda en profundidad: No necesita ningun parámetro.

busqueda = instancia_ejemplo.busqueda_en_profundidad()

#Busqueda en anchura: No necesita ningun paramétro.

#busqueda = instancia_ejemplo.busqueda_en_anchura()


""" En la busqueda a estrella, deberemos pasarle una heurística mediante una cadena con el nombre de la heurística.
    En este caso las heuristicas a elegir son:
        hSet: Heurística a nivel de conjunto
        hAdd: Heurística del coste aditivo
        hMax: Herísticas del coste máximo """

#busqueda = instancia_ejemplo.busqueda_A_estrella('hMax')
#busqueda = instancia_ejemplo.busqueda_A_estrella('hAdd')
#busqueda = instancia_ejemplo.busqueda_A_estrella('hSet')


#Busqueda óptima: No es necesario ningún parámetro.

#busqueda = instancia_ejemplo.busqueda_optima()


#Busqueda en profundidad acotada: Tendriamos que pasarle por parametro una cota

#busqueda = instancia_ejemplo.busqueda_en_profundidad_acotada()


#Busqueda iterativa: Tendriamos que pasarle por parametro una cotaFinal

#busqueda = instancia_ejemplo.busqueda_en_profundidad_iterativa()



"""Imprimimos el resultado de la busqueda"""

print(busqueda)


