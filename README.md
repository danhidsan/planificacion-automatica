El problema se encuentra en los siguientes ficheros:

    -problemaAscensoresPOO.py -> Resolución del problema mediante POO, creando una clase mediante la que podemos crear varias instancias del problema instanciando la clase ProblemaAscensores

    -problemaAscensores.py -> Resolución del problema unicamente con las librerías propuestas en práctica. Para crear varias instancias del problema tendríamos que hacer mas cambios en el código.

A parte, tenemos un archivo en el que se encuentran todas las instancias de la clase ProblemaAscensores. Este archivo se llama instancias.py.