import problema_planificación as probpl
import búsqueda_espacio_estados as búsqee
import math
from time import time

tiempo_inicial = time()

# Definimos los simbolos del problema

AscensoresLentos = ['a1', 'a2']
AscensoresRapidos = ['a3', 'a4']
Capacidades = ['0', '1', '2', '3']
Pasajeros = ['mujer', 'hombre']
Plantas = ['0', '1', '2', '3', '4', '5', '6', '7', '8']

# Implementamos la variables de estado

plantaPasajero = probpl.VariableDeEstados(nombre='plantaPasajero({p})',
                                          rango=Plantas,
                                          p=Pasajeros)
plantaAscensor = probpl.VariableDeEstados(nombre='plantaAscensor({a})',
                                          rango=Plantas,
                                          a=AscensoresLentos + AscensoresRapidos)
capacidad = probpl.VariableDeEstados(nombre='capacidad({a})',
                                     rango=Capacidades,
                                     a=AscensoresLentos + AscensoresRapidos)

# Definimos las relaciones rígidas

plantas_distintas = probpl.RelaciónRígida(lambda p1, p2: p1 != p2)

if len(Plantas) == 9:
    plantas_rapidos = probpl.RelaciónRígida(lambda p2: int(p2) % 2 == 0)
else:
    plantas_rapidos = probpl.RelaciónRígida(lambda p2: int(p2) % 4 == 0)

plantas_disponibles = probpl.RelaciónRígida(lambda a, p: (a, p) in [('a1', '0'),
                                                                    ('a1', '1'),
                                                                    ('a1', '2'),
                                                                    ('a1', '3'),
                                                                    ('a1', '4'),
                                                                    ('a2', '4'),
                                                                    ('a2', '5'),
                                                                    ('a2', '6'),
                                                                    ('a2', '7'),
                                                                    ('a2', '8')
                                                                    ])
capacidades_disponibles = probpl.RelaciónRígida(lambda a, c: (a, c) in [('a1', '0'),
                                                                        ('a1', '1'),
                                                                        ('a1', '2'),
                                                                        ('a1', '3'),
                                                                        ('a2', '0'),
                                                                        ('a2', '1'),
                                                                        ('a2', '2'),
                                                                        ('a3', '0'),
                                                                        ('a3', '1'),
                                                                        ('a3', '2'),
                                                                        ('a3', '3'),
                                                                        ('a4', '0'),
                                                                        ('a4', '1'),
                                                                        ('a4', '2'),
                                                                        ('a4', '3')
                                                                        ])

hay_sitio = probpl.RelaciónRígida(lambda c1: int(c1) != 0)
capacidad_disminuye = probpl.RelaciónRígida(lambda c1, c2: int(c1) - int(c2) == 1)
capacidad_aumenta = probpl.RelaciónRígida(lambda c1, c2: int(c1) - int(c2) == -1)

# Definimos el coste

coste_asc = probpl.CosteOperador(lambda a, p1, p2:
                                 {'a1': 6 + math.fabs(int(p1) - int(p2)), 'a2': 6 + math.fabs(int(p1) - int(p2)),
                                  'a3': 2 + 3 * 6 + math.fabs(int(p1) - int(p2)),
                                  'a4': 2 + 3 * 6 + math.fabs(int(p1) - int(p2))}[a])

# Definimos las acciones

asistir_pasajero = probpl.Operador(
    nombre='asistir_pasajero({pe}, {a}, {p1}, {p2})',
    precondiciones=[plantaPasajero({'{pe}': '{p1}'}),
                    plantaAscensor({'{a}': '{p2}'})],
    efectos= plantaAscensor({'{a}': '{p1}'}),
    relaciones_rigidas=[plantas_distintas('{p1}', '{p2}'),
                          plantas_disponibles('{a}', '{p2}'),
                          plantas_disponibles('{a}', '{p1}')],

    coste=coste_asc('{a}', '{p1}', '{p2}'),
    a=AscensoresRapidos + AscensoresLentos,
    p1=Plantas,
    p2=Plantas,
    pe=Pasajeros
)


mover_lento = probpl.Operador(
    nombre='mover_ascensorLento({a}, {p1}, {p2}, {pe})',
    precondiciones=[plantaAscensor({'{a}': '{p1}'}),
                    plantaPasajero({'{pe}': '{a}'})],
    efectos=plantaAscensor({'{a}': '{p2}'}),
    relaciones_rígidas=[plantas_distintas('{p1}', '{p2}'),
                        plantas_disponibles('{a}', '{p2}'),
                        plantas_disponibles('{a}', '{p1}')],
    coste=coste_asc('{a}', '{p1}', '{p2}'),
    a=AscensoresLentos,
    p1=Plantas,
    p2=Plantas,
    pe=Pasajeros
)

mover_rapido = probpl.Operador(
    nombre='mover_ascensorRapido({a}, {p1}, {p2}, {pe})',
    precondiciones=[plantaAscensor({'{a}': '{p1}'}),
                    plantaPasajero({'{pe}': '{a}'})],
    efectos=plantaAscensor({'{a}': '{p2}'}),
    relaciones_rígidas=[plantas_distintas('{p1}', '{p2}'),
                        plantas_rapidos('{p2}')],
    coste=coste_asc('{a}', '{p1}', '{p2}'),
    a=AscensoresRapidos,
    p1=Plantas,
    p2=Plantas,
    pe=Pasajeros
)



subirse_ascensor = probpl.Operador(
    nombre='subirse_ascensor({pe}, {a}, {p}, {c1}, {c2})',
    precondiciones=[capacidad({'{a}': '{c1}'}),
                    plantaAscensor({'{a}': '{p}'}),
                    plantaPasajero({'{pe}': '{p}'})],
    efectos=[capacidad({'{a}': '{c2}'}),
             plantaPasajero({'{pe}': '{a}'})],
    relaciones_rígidas=[hay_sitio('{c1}'),
                        capacidad_disminuye('{c1}', '{c2}'),
                        capacidades_disponibles('{a}', '{c2}')],
    a=AscensoresRapidos + AscensoresLentos,
    pe=Pasajeros,
    c1=Capacidades,
    c2=Capacidades,
    p=Plantas
)

bajarse_ascensor = probpl.Operador(
    nombre='bajarse_ascensor({pe}, {a}, {p}, {c1}, {c2})',
    precondiciones=[capacidad({'{a}': '{c1}'}),
                    plantaAscensor({'{a}': '{p}'}),
                    plantaPasajero({'{pe}': '{a}'})],
    efectos=[capacidad({'{a}': '{c2}'}),
             plantaPasajero({'{pe}': '{p}'})],
    relaciones_rígidas=[capacidad_aumenta('{c1}', '{c2}'),
                        capacidades_disponibles('{a}', '{c2}')],
    a=AscensoresRapidos + AscensoresLentos,
    pe=Pasajeros,
    c1=Capacidades,
    c2=Capacidades,
    p=Plantas
)

# Definimos el problema de problema de planificación

problema_ascensores = probpl.ProblemaPlanificación(
    variables_estados=[plantaAscensor,
                       plantaPasajero,
                       capacidad],
    operadores=[asistir_pasajero,
                mover_rapido,
                mover_lento,
                subirse_ascensor,
                bajarse_ascensor],
    estado_inicial=probpl.Estado(plantaAscensor({'a1': '3',
                                                 'a2': '6',
                                                 'a3': '0',
                                                 'a4': '4'}),
                                 plantaPasajero({'mujer': '4',
                                                 'hombre': '5'}),
                                 capacidad({'a1': '3',
                                            'a2': '2',
                                            'a3': '3',
                                            'a4': '3'})),
    objetivos=plantaPasajero({'mujer': '6',
                              'hombre': '8'})
)


# Heurísticas

def hSet(nodo):  # Heurística del nivel conjunto
    h = 0
    estado = nodo.estado
    if problema_ascensores.es_estado_final(estado):
        h = nodo.profundidad

    return h


def hMax(nodo):  # Heurística del coste máximo
    h = 0
    c = []
    estado = nodo.estado
    objetivos = problema_ascensores.objetivos
    asignaciones = estado.asignaciones
    for asig in asignaciones.items():
        for obj in objetivos.items():
            if asig == obj:
                c.append(nodo.profundidad)

    if not c:
        return h
    else:
        h = max(c)
        return h


def hAdd(nodo):  # Heurística del coste aditivo
    h = 0
    c = []
    estado = nodo.estado
    objetivos = problema_ascensores.objetivos
    asignaciones = estado.asignaciones
    for asig in asignaciones.items():
        for obj in objetivos.items():
            if asig == obj:
                c.append(nodo.profundidad)

    for res in c:
        h += res

    return h


def hitos(nodo):
    h = 0
    estado = nodo.estado
    objetivos = problema_ascensores.objetivos.items()
    print(type(estado.asignaciones))


    return h


# Busquedas


#busqueda_AEstrella = búsqee.BúsquedaAEstrella(hitos,detallado=True)
#busqueda = busqueda_AEstrella.buscar(problema_ascensores)



busqueda_profundidad = búsqee.BúsquedaEnProfundidad(detallado=True)
busqueda = busqueda_profundidad.buscar(problema_ascensores)


#busqueda_anchura = búsqee.BúsquedaEnAnchura(detallado=True)
#busqueda = busqueda_anchura.buscar(problema_ascensores)

print(busqueda)



tiempo_final = time()

tiempo_ejecucion = tiempo_final - tiempo_inicial

print(tiempo_ejecucion)
