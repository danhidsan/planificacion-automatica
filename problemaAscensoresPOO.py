import problema_planificación as probpl
import búsqueda_espacio_estados as búsqee
import math
import time


class ProblemaAscensores():
    def __init__(self, ascensoresLentos, ascensoresRapidos, capacidadesAscensores
                 , pasajeros, plantas, plantasEstadoInicial, plantasEstadoFinal, plantaAscensoresEstadoInicial):

        #Simbolos del problema, que serán con los que instanciemos al objeto ProblemaAscensores

        self.AscensoresLentos = ascensoresLentos
        self.AscensoresRapidos = ascensoresRapidos
        self.CapacidadesAscensores = capacidadesAscensores
        self.Capacidades = [str(x) for x in range(max(self.CapacidadesAscensores) + 1)]
        self.Pasajeros = pasajeros
        self.Plantas = plantas
        self.PlantasEstadoInicial = plantasEstadoInicial
        self.PlantasEstadoFinal = plantasEstadoFinal
        self.PlantaAscensoresEstadoInicial = plantaAscensoresEstadoInicial

        #Variables de Estado

        self.plantaPasajero = probpl.VariableDeEstados(nombre='plantaPasajero({p})',
                                                       rango=self.Plantas,
                                                       p=self.Pasajeros)
        self.plantaAscensor = probpl.VariableDeEstados(nombre='plantaAscensor({a})',
                                                       rango=self.Plantas,
                                                       a=self.AscensoresLentos + self.AscensoresRapidos)
        self.capacidad = probpl.VariableDeEstados(nombre='capacidad({a})',
                                                  rango=self.Capacidades,
                                                  a=self.AscensoresLentos + self.AscensoresRapidos)

        #Relaciones Rígidas

        self.plantas_distintas = probpl.RelaciónRígida(lambda p1, p2: p1 != p2)

        if len(self.Plantas) == 9:
            self.plantas_rapidos = probpl.RelaciónRígida(lambda p2: int(p2) % 2 == 0)
        else:
            self.plantas_rapidos = probpl.RelaciónRígida(lambda p2: int(p2) % 4 == 0)

        self.plantas_disponibles = probpl.RelaciónRígida(lambda a, p: (a, p) in self.asignaPlantasLentos())


        self.capacidades_disponibles = probpl.RelaciónRígida(lambda a, c: (a, c) in self.asignaCapacidades())





        self.hay_sitio = probpl.RelaciónRígida(lambda c1: int(c1) != 0)
        self.capacidad_disminuye = probpl.RelaciónRígida(lambda c1, c2: int(c1) - int(c2) == 1)
        self.capacidad_aumenta = probpl.RelaciónRígida(lambda c1, c2: int(c1) - int(c2) == -1)

        self.coste_asc = probpl.CosteOperador(lambda a, p1, p2: self.asignaCostes(p1, p2)[a])

        self.asistir_pasajero = probpl.Operador(
            nombre='asistir_pasajero({pe}, {a}, {p1}, {p2})',
            precondiciones=[self.plantaPasajero({'{pe}': '{p1}'}),
                            self.plantaAscensor({'{a}': '{p2}'})],
            efectos=self.plantaAscensor({'{a}': '{p1}'}),
            relaciones_rigidas=[self.plantas_distintas('{p1}', '{p2}'),
                                self.plantas_disponibles('{a}', '{p2}'),
                                self.plantas_disponibles('{a}', '{p1}')],

            coste=self.coste_asc('{a}', '{p1}', '{p2}'),
            a=self.AscensoresRapidos + self.AscensoresLentos,
            p1=self.Plantas,
            p2=self.Plantas,
            pe=self.Pasajeros
        )

        self.mover_lento = probpl.Operador(
            nombre='mover_ascensorLento({a}, {p1}, {p2}, {pe})',
            precondiciones=[self.plantaAscensor({'{a}': '{p1}'}),
                            self.plantaPasajero({'{pe}': '{a}'})],
            efectos=self.plantaAscensor({'{a}': '{p2}'}),
            relaciones_rígidas=[self.plantas_distintas('{p1}', '{p2}'),
                                self.plantas_disponibles('{a}', '{p2}'),
                                self.plantas_disponibles('{a}', '{p1}')],
            coste=self.coste_asc('{a}', '{p1}', '{p2}'),
            a=self.AscensoresLentos,
            p1=self.Plantas,
            p2=self.Plantas,
            pe=self.Pasajeros
        )

        self.mover_rapido = probpl.Operador(
            nombre='mover_ascensorRapido({a}, {p1}, {p2}, {pe})',
            precondiciones=[self.plantaAscensor({'{a}': '{p1}'}),
                            self.plantaPasajero({'{pe}': '{a}'})],
            efectos=self.plantaAscensor({'{a}': '{p2}'}),
            relaciones_rígidas=[self.plantas_distintas('{p1}', '{p2}'),
                                self.plantas_rapidos('{p2}')],
            coste=self.coste_asc('{a}', '{p1}', '{p2}'),
            a=self.AscensoresRapidos,
            p1=self.Plantas,
            p2=self.Plantas,
            pe=self.Pasajeros
        )

        self.subirse_ascensor = probpl.Operador(
            nombre='subirse_ascensor({pe}, {a}, {p}, {c1}, {c2})',
            precondiciones=[self.capacidad({'{a}': '{c1}'}),
                            self.plantaAscensor({'{a}': '{p}'}),
                            self.plantaPasajero({'{pe}': '{p}'})],
            efectos=[self.capacidad({'{a}': '{c2}'}),
                     self.plantaPasajero({'{pe}': '{a}'})],
            relaciones_rígidas=[self.hay_sitio('{c1}'),
                                self.capacidad_disminuye('{c1}', '{c2}'),
                                self.capacidades_disponibles('{a}', '{c2}')],
            a=self.AscensoresRapidos + self.AscensoresLentos,
            pe=self.Pasajeros,
            c1=self.Capacidades,
            c2=self.Capacidades,
            p=self.Plantas
        )

        self.bajarse_ascensor = probpl.Operador(
            nombre='bajarse_ascensor({pe}, {a}, {p}, {c1}, {c2})',
            precondiciones=[self.capacidad({'{a}': '{c1}'}),
                            self.plantaAscensor({'{a}': '{p}'}),
                            self.plantaPasajero({'{pe}': '{a}'})],
            efectos=[self.capacidad({'{a}': '{c2}'}),
                     self.plantaPasajero({'{pe}': '{p}'})],
            relaciones_rígidas=[self.capacidad_aumenta('{c1}', '{c2}'),
                                self.capacidades_disponibles('{a}', '{c2}')],
            a=self.AscensoresRapidos + self.AscensoresLentos,
            pe=self.Pasajeros,
            c1=self.Capacidades,
            c2=self.Capacidades,
            p=self.Plantas
        )

        self.problema_ascensores = probpl.ProblemaPlanificación(
            variables_estados=[self.plantaAscensor,
                               self.plantaPasajero,
                               self.capacidad],
            operadores=[self.asistir_pasajero,
                        self.mover_lento,
                        self.mover_rapido,
                        self.subirse_ascensor,
                        self.bajarse_ascensor],
            estado_inicial=probpl.Estado(self.plantaAscensor(dict(zip(self.AscensoresLentos + self.AscensoresRapidos,
                                                                      self.PlantaAscensoresEstadoInicial))),
                                         self.plantaPasajero(dict(zip(self.Pasajeros, self.PlantasEstadoInicial))),
                                         self.capacidad(dict(zip(self.AscensoresLentos + self.AscensoresRapidos,
                                                                 [str(x) for x in self.CapacidadesAscensores])))),
            objetivos=self.plantaPasajero(dict(zip(self.Pasajeros, self.PlantasEstadoFinal)))
        )


    # Heurísticas

    def hSet(self, nodo):  # Heurística del nivel conjunto
        h = 0
        estado = nodo.estado
        print(estado.asignaciones)
        if self.problema_ascensores.es_estado_final(estado):
            h = nodo.profundidad

        return h

    def hMax(self, nodo):  # Heurística del coste máximo
        h = 0
        c = []
        estado = nodo.estado
        objetivos = self.problema_ascensores.objetivos
        asignaciones = estado.asignaciones
        for asig in asignaciones.items():
            for obj in objetivos.items():
                if asig == obj:
                    c.append(nodo.profundidad)

        if not c:
            return h
        else:
            h = max(c)
            return h

    def hAdd(self, nodo):  # Heurística del coste aditivo
        h = 0
        c = []
        estado = nodo.estado
        objetivos = self.problema_ascensores.objetivos
        asignaciones = estado.asignaciones
        for asig in asignaciones.items():
            for obj in objetivos.items():
                if asig == obj:
                    c.append(nodo.profundidad)

        for res in c:
            h += res

        return h


    # Algoritmos de búsqueda

    def busqueda_en_profundidad(self):
        tiempo_inicial = time.time()

        busqueda_profundidad = búsqee.BúsquedaEnProfundidad(detallado=True)
        busqueda = busqueda_profundidad.buscar(self.problema_ascensores)

        tiempo_final = time.time()

        tiempo_ejecucion = tiempo_final - tiempo_inicial

        print('\n\nTiempo de ejecución: ', tiempo_ejecucion)

        return busqueda

    def busqueda_en_anchura(self):

        tiempo_inicial = time.time()

        busqueda_anchura = búsqee.BúsquedaEnAnchura(detallado=True)
        busqueda = busqueda_anchura.buscar(self.problema_ascensores)

        tiempo_final = time.time()

        tiempo_ejecucion = tiempo_final - tiempo_inicial

        print('\n\nTiempo de ejecución: ', tiempo_ejecucion)

        return busqueda

    def busqueda_A_estrella(self, heuristica):

        tiempo_inicial = time.time()

        heur = ''

        """Aqui solo contemplamos las heuristicas que tenemos implementadas"""

        if heuristica == 'hSet':
            heur = self.hSet
        elif heuristica == 'hMax':
            heur = self.hAdd
        elif heuristica == 'hAdd':
            heur = self.hMax

        busqueda_AEstrella = búsqee.BúsquedaAEstrella(heur, detallado=True)
        busqueda = busqueda_AEstrella.buscar(self.problema_ascensores)

        tiempo_final = time.time()

        tiempo_ejecucion = tiempo_final - tiempo_inicial

        print('\n\nTiempo de ejecución: ', tiempo_ejecucion)

        return busqueda

    def busqueda_en_profundidad_acotada(self,cota):
        tiempo_inicial = time.time()

        busqueda_profundidad_acotada = búsqee.BúsquedaEnProfundidadAcotada(cota, detallado=True)
        busqueda = busqueda_profundidad_acotada.buscar(self.problema_ascensores)

        tiempo_final = time.time()

        tiempo_ejecucion = tiempo_final - tiempo_inicial

        print('\n\nTiempo de ejecución: ', tiempo_ejecucion)

        return busqueda

    def busqueda_en_profundidad_iterativa(self, cotaFinal):
        tiempo_inicial = time.time()

        busqueda_profundidad_iterativa = búsqee.BúsquedaEnProfundidadIterativa(cotaFinal, detallado=True)
        busqueda = busqueda_profundidad_iterativa.buscar(self.problema_ascensores)

        tiempo_final = time.time()

        tiempo_ejecucion = tiempo_final - tiempo_inicial

        print('\n\nTiempo de ejecución: ', tiempo_ejecucion)

        return busqueda

    def busqueda_primero_el_mejor(self, f):
        tiempo_inicial = time.time()

        busqueda_primero_el_mejor = búsqee.BúsquedaPrimeroElMejor(f, detallado=True)
        busqueda = busqueda_primero_el_mejor.buscar(self.problema_ascensores)

        tiempo_final = time.time()

        tiempo_ejecucion = tiempo_final - tiempo_inicial

        print('\n\nTiempo de ejecución: ', tiempo_ejecucion)

        return busqueda

    def busqueda_optima(self):
        tiempo_inicial = time.time()

        busqueda_optima = búsqee.BúsquedaÓptima(detallado=True)
        busqueda = busqueda_optima.buscar(self.problema_ascensores)

        tiempo_final = time.time()

        tiempo_ejecucion = tiempo_final - tiempo_inicial

        print('\n\nTiempo de ejecución: ', tiempo_ejecucion)

        return busqueda





    # Funciones Auxiliares

    def asignaCapacidades(self):
        lista_res = []
        ascensores = self.AscensoresLentos + self.AscensoresRapidos
        for ascensor in ascensores:
            capacidad = int(self.CapacidadesAscensores[ascensores.index(ascensor)])
            for i in range(capacidad + 1):
                lista = (ascensor, str(i))
                lista_res.append(lista)

        return lista_res

    def asignaPlantasLentos(self):
        list_res = []

        num = int(len(self.Plantas) / len(self.AscensoresLentos))
        num2 = 0

        for ascensor in self.AscensoresLentos:
            i = num2
            while i <= num:
                list_res.append((ascensor, self.Plantas[i]))
                i += 1

            num2 = num
            num += int(len(self.Plantas) / len(self.AscensoresLentos))

        return list_res

    def asignaCostes(self, p1, p2):
        dict = {}
        for ascensor in self.AscensoresLentos:
            dict[ascensor] = 6 + math.fabs(int(p1) - int(p2))

        for ascensor in self.AscensoresRapidos:
            dict[ascensor] = 2 + 3 * 6 + math.fabs(int(p1) - int(p2))

        return dict



